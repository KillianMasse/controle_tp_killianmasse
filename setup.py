import setuptools
from distutils.core import setup

setup(
    name='TestGrid',
    version='0.0.1',
    license="GNU GPLv3",
    author="Killian Massé",
    description="TestGrid is a simple package \
    in order to make some test on packaging principles in Python",
    python_requires=">=3.4",
    long_description=open('README.md').read(),
    package_dir={"": "robot"},
    packages=setuptools.find_namespace_packages(where="robot")

)
