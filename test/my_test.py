#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
author : killian masse

"""

import unittest
import logging

from Robot.map import Grid


class DeplacementBasTest(unittest.TestCase):
    """
    Class deplacement gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.robot = Grid()

    def test_deplacement_bas(self):
        """ deplacement du robot vers le bas de la grille """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.down(grille1, 3, 0, 0)
        self.robot.affiche(grille1)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")

    def test_deplacement_bas_imp(self):
        """ deplacement du robot vers le bas de la grille impossible car tout en bas au deuxième down """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.down(grille1, 3, 0, 0)
        self.robot.down(grille1, 3, 1, 0)
        self.robot.down(grille1, 3, 2, 0)
        self.robot.affiche(grille1)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")


class DeplacementHautTest(unittest.TestCase):
    """
    Class deplacement gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.robot = Grid()

    def test_deplacement_haut_imp(self):
        """ deplacement du robot vers le haut de la grille impossible (car départ) """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.up(grille1, 0, 0)
        self.robot.affiche(grille1)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")
        

    def test_deplacement_haut(self):
        """ deplacement du robot vers le bas de la grille """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.down(grille1, 3, 0, 0)
        self.robot.up(grille1, 1, 0)
        self.robot.affiche(grille1)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")


class DeplacementDroiteTest(unittest.TestCase):
    """
    Class deplacement gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.robot = Grid()

    def test_deplacement_droite_imp(self):
        """ deplacement du robot vers la droite de la grille impossible car au bout après le deuxième right """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.right(grille1, 3, 0, 0)
        self.robot.right(grille1, 3, 0, 1)
        self.robot.right(grille1, 3, 0, 2)
        self.robot.affiche(grille1)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")

    def test_deplacement_droite(self):
        """ deplacement du robot vers la droite de la grille """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.right(grille1, 3, 0, 0)
        self.robot.affiche(grille1)


class DeplacementGaucheTest(unittest.TestCase):
    """
    Class deplacement gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.robot = Grid()

    def test_deplacement_gauche_imp(self):
        """ deplacement du robot vers la gauche de la grille impossible car départ """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.left(grille1, 0, 0)
        self.robot.affiche(grille1)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")

    def test_deplacement_gauche(self):
        """ deplacement du robot vers la gauche de la grille """
        grille1 = self.robot.grille(3, 3)
        self.robot.affiche(grille1)
        self.robot.right(grille1, 3, 0, 0)
        self.robot.left(grille1, 0, 1)
        self.robot.affiche(grille1)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")


if __name__ == "__main__":
    unittest.main()
