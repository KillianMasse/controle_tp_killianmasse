# RENDU CONTROLE TP ADMCO

## Objectif
Le but est de palcer un 1 dans une grille pour représenterla présence d’un robot. Au débutle robot se trouve à la position 0,0 de la grille. Les méthodes permettent  de déplacerle robot. En cas d’impossibilité de se déplacer (présence d’un bord) le robot reste à sa place.

## Trvail effectué
J'ai créé un package **Robot** dans lequel se trouve un fichier **map.py** dans lequel il y a ma classe **Grid** qui établit les différentes fonctions de déplacement du robot et d'affichage de la grille.
J'ai aussi créé un package **test** dans lequel se trouve le fichier **my_test.py** contenant mes différents tets unitaires.
J'ai ajouté un fichier **gitlab-ci.yml** afin de réaliser l'intégration continue
Réalisation d'un fichier **setup.py** pour l'archivage

## Notation
Les différents codes ont été vérifiés à l'aide de **pylint** et **black** afin d'avoir la meilleure notation possible. De plus, le code **.gitlab-ci.yml** a permis de vérifié sous **pycharm** le bon fonctionnement de **my_test.py** (tests unitaires)

## Problème
J'ai voulu utiliser numpy, j'ai donc effectué l'installation mais bien que pycharm le reconnaise, ce n'est pas le cas de pytest. Le manque de temps m'a empêché de corriger cette erreur.

## Video
Une video de mon travail est disponible sur le repository : **kazam_killian.movie**

## Dossier
Le TP est disponible à cet adresse :
https://gitlab.com/KillianMasse/controle_tp_killianmasse
