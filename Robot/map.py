# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 12:43:34 2020
@author: Killian Massé

Ajout de test sur les types
"""

import numpy as np


class Grid:
    """
    class Grid
    """

    def __init__(self):
        """ """

    def grille(self, _nb_lines, _nb_columns):
        """
        fonction permettant de créer une grille de la taille souhaitée
        """
        if isinstance(_nb_lines, int) and isinstance(_nb_columns, int):
            _grid = np.zeros((_nb_lines, _nb_columns))
            _grid[0][0] = 1
            return _grid
        return "ERROR"

    def up(self, _grid, x_robot, y_robot):
        """
        fonction permettant au robot de monter d'une case
        """
        if isinstance(x_robot, int) and isinstance(y_robot, int):
            _grid[x_robot][y_robot] = 0
            if x_robot > 0:
                x_robot = x_robot - 1
                _grid[x_robot][y_robot] = 1
                return x_robot
            return x_robot
        return "ERROR"

    def down(self, _grid, _nb_lines, x_robot, y_robot):
        """
        fonction permettant au robot de descendre d'une case
        """
        if isinstance(x_robot, int) and isinstance(y_robot, int):
            _grid[x_robot][y_robot] = 0
            if x_robot < _nb_lines - 1:
                x_robot = x_robot + 1
                _grid[x_robot][y_robot] = 1
                return x_robot
            return x_robot
        return "ERROR"

    def left(self, _grid, x_robot, y_robot):
        """
        fonction permettant au robot d'aller d'une case vers la gauche
        """
        if isinstance(x_robot, int) and isinstance(y_robot, int):
            _grid[x_robot][y_robot] = 0
            if y_robot > 0:
                y_robot = y_robot - 1
                _grid[x_robot][y_robot] = 1
                return y_robot
            return y_robot
        return "ERROR"

    def right(self, _grid, _nb_columns, x_robot, y_robot):
        """
        fonction permettant au robot d'aller d'une case vers la droite
        """
        if isinstance(x_robot, int) and isinstance(y_robot, int):
            _grid[x_robot][y_robot] = 0
            if y_robot < _nb_columns - 1:
                y_robot = y_robot + 1
                _grid[x_robot][y_robot] = 1
                return y_robot
            return y_robot
        return "ERROR"

    def affiche(self, _grid):
        """
        fonction permettant d'afficher la grille où se trouve le robot
        """
        return _grid
